package Kol::Controller::Viewer;

use v5.40;
use Mojo::Base 'Mojolicious::Controller', -async_await;

use Mojo::DOM;
use Mojo::URL;
use Crypt::Digest::SHA256 'sha256_b64u';
use Encode 'decode';
use Digest::MD5 'md5_hex';

sub index ($self) {
    $self->render('index');
}

async sub viewer ($self) {
    my $url = $self->req->url;
    my $pq = $url->path_query;

    my $ua = Mojo::UserAgent->new(
        connect_timeout    => 120,
        inactivity_timeout => 120,
        request_timeout    => 120,
    );
    if ($pq =~ /^\/https?\:\/\//) {
        $pq =~ s/^\///;
        my $md5 = md5_hex $pq;
        my $text = $self->chi->compute(
            sha256_b64u($pq),
            30 * 24 * 3_600,
            sub {
                my $resp = $ua->get($pq, {
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/117.0',
                })->result;
                $resp->is_success or return $self->render(
                    text   => "Couldn't download page. Error: " . $resp->code,
                    status => $resp->code,
                );

                if ($resp->headers->content_type =~ /^text\/html\b/) {
                    my $dom = Mojo::DOM->new(decode 'UTF-8', $resp->body);
                    $dom->find('script')->each(sub { $_->remove });
                    $dom->find('p, h1, h2, h3, h4, h5, h6')->each(sub ($e, $num) {
                        if (! length $e->attr('id')) {
                            $e->attr(id => "__kolgr-$num");
                        }
                    });
                    $dom->find('img')->each(sub {
                        my $orig_url = $_->attr('src') or return;
                        my $new_url = Mojo::URL->new($orig_url)->base(Mojo::URL->new($pq))->to_abs->to_string;
                        $_->attr(src => $new_url);
                    });
                    $dom->at('body')->append_content(<<~"JAVASCRIPT");
                        <script>
                        let scrolled = false;
                        document.addEventListener('scroll', (event) => {
                            scrolled = true;
                        });
                        const urlMd5 = '$md5';
                        const savedId = localStorage.getItem('savedId-' + urlMd5);
                        if (savedId) {
                            document.getElementById(savedId).scrollIntoView();
                        }

                        const ps = document.querySelectorAll('p, h1, h2, h3, h4, h5, h6');
                        setInterval(() => {
                            if (! scrolled) return;
                            scrolled = false;
                            let lower = 0, upper = ps.length - 1;
                            let lowest = upper;
                            while (lower < upper) {
                                const middle = Math.floor((lower + upper) / 2);
                                let el = ps[middle];
                                let rect = el.getBoundingClientRect();
                                let bottom = rect.bottom;
                                if (bottom > 0) {
                                    if (middle < lowest) lowest = middle;
                                    upper = middle;
                                } else {
                                    lower = middle + 1;
                                }
                            }
                            const p = ps[Math.min(lowest, lower)];
                            localStorage.setItem('savedId-' + urlMd5, p.id);
                        }, 1e3);
                        </script>
                    JAVASCRIPT

                    return $dom->to_string;
                }
            },
        );
        return $self->render(text => $text);
    } else {
        $self->render(json => [10, 20, 30]);
    }
}

sub redir ($self) {
    my $url = $self->param('url');

    $self->redirect_to("/$url");
}
