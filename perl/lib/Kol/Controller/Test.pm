package Kol::Controller::Test;

use v5.40;
use Mojo::Base 'Mojolicious::Controller';

sub test ($self) {
    my $headers = $self->req->headers->to_string;

    $self->render(text => $headers, format => 'txt');
}