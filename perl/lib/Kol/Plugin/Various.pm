package Kol::Plugin::Various;

use v5.40;
use Mojo::Base 'Mojolicious::Plugin';

use CHI;

sub register ($self, $app, $config) {
    $app->helper(chi => sub ($c) {
        state $cache = CHI->new(
            driver             => 'File',
            root_dir           => $c->config->{data_dir} . '/cache',
            # compress_threshold => 1024 ** 2,
        );
    });
}
