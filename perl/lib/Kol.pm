package Kol;
use Mojo::Base 'Mojolicious', -signatures;

# This method will run once at server start
sub startup ($self) {

  # Load configuration from config file
  push $self->plugins->namespaces->@*, 'Kol::Plugin';
  my $config = $self->plugin('Config');

  # Configure the application
  $self->secrets($config->{secrets});

  $self->plugin('Various');

  # Router
  my $r = $self->routes;

  $r->get('/test')->to('Test#test');
  $r->get('/')->to('Viewer#index');
  $r->get('/*')->to('Viewer#viewer');
  $r->post('/redirect')->to('Viewer#redir');
}

1;
