#!/usr/bin/env perl

use v5.40;
use FindBin '$RealBin';
use lib "$RealBin/../../local/lib/perl5", "$RealBin/../../lib";

use Kol;

my $app = Kol->new;

$app->chi->purge;
