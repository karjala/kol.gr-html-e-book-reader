#!/usr/bin/env perl

use v5.40;
use FindBin '$RealBin';
use lib "$RealBin/local/lib/perl5", "$RealBin/lib";

use File::Find 'find';

chdir $RealBin;

find(sub {
    my $full_path = $File::Find::name;
    return unless -f $full_path and $full_path =~ /\.pm\z/;
    my $module_name = $full_path =~ s/^\Q$RealBin\E\/lib\/|\.pm\z//gr;
    $module_name =~ s/\//::/g;
    # say $module_name;
    eval "use $module_name;";
    die $@ if $@;
}, "$RealBin/lib");
