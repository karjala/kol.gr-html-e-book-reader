#!/usr/bin/env perl

use v5.40;
use FindBin '$RealBin';

chdir "$RealBin/perl";

$ENV{MOJO_REVERSE_PROXY} = 1;

exec 'carton exec -- morbo -w . -v script/kol';
